from anadama.sge import SGEContext

ctx = SGEContext(queue="general")
ctx.do("cp #{/network/share/sequences.fasta} @{raw.fasta}")
ctx.sge_do("quality_filter #{raw.fasta} @{clean.fasta}",
           mem=5000, time=90)
ctx.sge_do("taxonomic_profile #{clean.fasta} @{profile.txt}",
           mem=3000, time=60)
ctx.sge_do("taxonomic_profile #{raw.fasta} @{profile.txt}",
           mem=3000, time=70)
ctx.go(n_sge_parallel=2)
